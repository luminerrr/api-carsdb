const { Cars } = require('./models');

module.exports = {
    create: async (req,res) =>{
        const body = req.body;
        try {
            const data = await Cars.create(body);

            return res.status(200).json({
                success : true,
                error : false,
                data,
                message : 'data successfully created'

            })
        } catch (error) {
            return res.status(400).json({
                success : false,
                error : true,
                data : null,
                message : 'data fail created'
            })
            
        }
    },

    find: async (req,res)=>{
        const body = req.body;
        try {
            const data = await Cars.findAll()

            return res.status(200).json({
                success : true,
                error : false,
                data,
                message : 'data find'
            })
        } catch (error) {
            return res.status(400).json({
                success : false,
                error : true,
                data : null,
                message : 'no data found, failed!'
            })
        }

    },

    update: async (req,res) =>{
        try {
            const body = req.body;
            const data = await Cars.update(body, {where:{
                id:req.params.id
            }})
            return res.status(200).json({
                success : true,
                error : false,
                data,
                message: 'data successfully updated'
            })
        } catch (error) {
            return res.status(400).json({
                success:false,
                error,
                data : null,
                message : 'data update failed'
            })
            
        }
    },
    destroy : async (req,res)=>{
        const body = req.body;
        try {
            const data = await Cars.destroy({where:{id:req.params.id}})

            return res.status(200).json({
                success : true,
                error : false,
                data,
                message : 'data deleted'
            })
        } catch (error) {
            return res.status(400).json({
                success : false,
                error : true,
                data : null,
                message : 'no data deleted, failed!'
            })
        }

    },

}