const express = require('express');
const app = express();
const Cars = require('./models');
const multer = require('multer');
const {create, find} = require('./control');
const PORT  = process.env.PORT || 3001;

// JSON Middleware
app.use(express.json())

app.get('/',create);

//Get all cars
app.get('/cars', find);



app.listen(PORT, ()=>{
    console.log(`server listening on http://localhost:${PORT}`);
})