'use strict';



module.exports = {
  async up (queryInterface, DataTypes) {
    return Promise.all([
      queryInterface.addColumn(
        'Cars', //table name
        'img', //new column
        {
          type:DataTypes.BLOB('long')
        }
      )
    ])
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
  },

  async down (queryInterface, Sequelize) {
    return Promise.all([
      queryInterface.removeColumn('Cars','img')
    ])
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
